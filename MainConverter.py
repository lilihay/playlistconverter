import json
import requests
from Secrets import user_id, playlist_id
from refresh import Refresh


class saveSongs:
    def __init__(self):
        self.user_id = user_id
        self.spotify_token = ""
        self.playlist_id = playlist_id
        self.tracks_names = ""
        self.list_names = []

    def find_songs(self):
        # Look throw playlist and add to list
        qurry = "https://api.spotify.com/v1/playlists/{}/tracks".format(playlist_id)

        response = requests.get(qurry, headers={"Content-type": "application/json",
                                                'Authorization': "Bearer {}".format(self.token)})
        response_json = response.json()

        for i in response_json["items"]:
            self.tracks_names += (i['track']['name'] + ',')
        self.tracks_names = self.tracks_names[:-1]
        print(self.tracks_names)
        self.list_names = self.tracks_names.split(',')
        print(self.list_names)

    def refresh(self):
        refresh_call = Refresh()

        self.token = refresh_call.refresh()

        self.find_songs()


a = saveSongs()
a.refresh()
